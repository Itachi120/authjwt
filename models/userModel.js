const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const userSchema = mongoose.Schema(
    {
        login : {
            type : String,
            required : true,
            unique : true
        },
        pass : {
            type : String,
            required : true,
            unique : true
        },
    },
    {
        timestamps : true
    }
)

userSchema.pre('save',async function(){
    console.log("Hook atteint",this);
    this.pass = await bcrypt.hash(this.pass,parseInt(process.env.BCRYPT_SALT_COUNT))
    //console.log("Hook atteint",this);
})

const User = mongoose.model('UserModel',userSchema)

module.exports = User;