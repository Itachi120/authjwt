const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller')
const verifyTokenMiddleware = require('../jsonwebtoken/verify')

router.use(verifyTokenMiddleware.verifyTokenMiddleware)
//get
router.get('/all',userController.getAllUser)
router.get('/:id',userController.getUserByID)

//put
router.put('/update/',userController.updateUserbyID)
router.put('/update/delete/:id',userController.updateUserbyID)


module.exports = router;