const express =  require('express')
const app =  express()
const mongoose = require('mongoose')
const cors = require('cors')
const config = require('./config')
const verifyTokenMiddleware = require('./jsonwebtoken/verify')
//router
const userRouter = require('./routes/userRouter')
const setRouter = require('./routes/setUserRouter')
const authRouter = require('./routes/authRouter')
//app use
app.use(express.json())
app.use(cors())
app.use(express.static(__dirname + '/public'));
//api routage
app.use('/api/auth',authRouter)
app.use('/api/user', verifyTokenMiddleware.verifyTokenMiddleware,userRouter)
app.use('/api/set',setRouter)

mongoose.connect('mongodb://localhost:27017/authjwt')
    .then(() => {
        console.log('database connected');
    }).catch((e) => {
        console.log(e);
    })


app.listen(process.env.SERVER_PORT,()=>{
    console.log("Node api running on port " + process.env.SERVER_PORT);
})