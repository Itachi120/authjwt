const jwt = require('jsonwebtoken')

const extractBearer = authorization => {
    if(typeof authorization !== 'string'){
        return false
    }

    const matches = authorization.match(/(bearer)\s+(\S+)/i)

    return matches && matches[2]
}

const verifyTokenMiddleware = async (req,res,next)=>{
    const token = req.headers.authorization && extractBearer(req.headers.authorization)
    //console.log('AUTHORIZATION :', req.headers);
    //console.log('TOKEN :', token);
    if (!token) {
        return res.status(401).json({message : "Token non retrouvé"})
    }

    //validité du token
    jwt.verify(token , process.env.JWT_SECRET,(err,decodedToken)=>{
        if (err) {
         return res.status(401).json({message : "Mauvais token"})
        }
        next()
    })
}


module.exports = {
    verifyTokenMiddleware
}