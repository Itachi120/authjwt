const User = require('../models/userModel')
const bcrypt = require('bcrypt')

const setUser = async (req,res)=>{
    try{
        let dataUser = req.body
        console.log(dataUser);
        const testExist = await User.findOne({login: req.body.login})
        if (testExist) {
            res.status(200).json({message : "Cet utilisateur existe déjà"})
        }else{
            console.log({data : req.body});
            const user = await User.create(req.body)
            res.status(200).json({data : user});
        }

    }catch(error){
        console.log("Message d'erreur",error.message.split(':')[0]);
        if (error.message.split(':')[0] === 'User validation failed') {
            res.status(500).json({
                message : "Validation user failed",
                error : error.message,
            })
        }else{
            res.status(500).json({
                error : error.message,
            })
        }

    }
}


const getAllUser= async (req,res)=>{
    try{
        const user = await User.find({})
        res.status(200).json({data : user});
    }catch(error){
        console.log(error.message);
        res.status(500).json({message : error.message})
    }
}


const getUserByID = async (req,res)=>{
    try{
        const id = req.params.id
        console.log(id);
        const user = await User.findById(id)
        res.status(200).json({data : user});
    }catch(error){
        console.log(error.message);
        res.status(500).json({message : error.message})
    }
}


const updateUserbyID = async (req,res)=>{
    try{
        const id = req.body.id
        delete req.body.id
        if(req.body.pass !== ""){
            req.body.pass = await bcrypt.hash(req.body.motDePasse,parseInt(process.env.BCRYPT_SALT_COUNT))
            const upMDP = await User.findByIdAndUpdate(id,{pass : req.body.pass})
        }
        delete req.body.pass
        const user = await User.findByIdAndUpdate(id,req.body)
        res.status(200).json({data : "Data updated"});
    }catch(error){
        console.log(error.message);
        res.status(500).json({message : error.message})
    }
}

const deleteUserbyID = async (req,res)=>{
    try{
        const id = req.params.id
        const user = await User.findByIdAndUpdate(id,req.body)
        res.status(200).json({data : "Data updated"});
    }catch(error){
        console.log(error.message);
        res.status(500).json({message : error.message})
    }
}




module.exports = {
    setUser,
    getAllUser,
    getUserByID,
    updateUserbyID,
    deleteUserbyID
}