const User = require('../models/userModel')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const authUser = async (req,res)=>{
    try{
        const username = req.body.login
        const password = req.body.pass
        const user = await User.findOne({login : username})
        if(!user){
            res.status(401).json({message : "Utilisateur inexistant"})
        }else{
            const match = await bcrypt.compare(password,user.pass)
            if (match) {
                //token generate
                const token = jwt.sign({
                    id : user._id,
                    login : user.login,
                },process.env.JWT_SECRET,{expiresIn : process.env.JWT_DURING})
                res.status(200).json({data : user,access_token : token})
            }
            else{
                return res.status(403).send("Mot de passe incorrect")
            }
        }

    }catch(error){
        console.log(error.message);
        res.status(500).json({message : error.message})
    }
}


module.exports = {
    authUser
}